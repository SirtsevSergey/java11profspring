package com.sber.study.library.java11libraryproject.project.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sber.study.library.java11libraryproject.project.dto.AuthorDTO;
import com.sber.study.library.java11libraryproject.project.jwtsecurity.JwtTokenUtil;
import com.sber.study.library.java11libraryproject.project.model.Author;
import com.sber.study.library.java11libraryproject.project.service.userdetails.CustomUserDetailsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class AuthorControllerTest {
    
    @Autowired
    public MockMvc mvc;
    
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    
    @Autowired
    private CustomUserDetailsService customUserDetailsService;
    
    private String token;
    
    private static final String ROLE_USER_NAME = "andy_user";
    
    private HttpHeaders generateHeaders() {
        HttpHeaders headers = new HttpHeaders();
        token = generateToken(ROLE_USER_NAME);
        headers.add("Authorization", "Bearer " + token);
        return headers;
    }
    
    private String generateToken(String userName) {
        return jwtTokenUtil.generateToken(customUserDetailsService.loadUserByUsername(userName));
    }
    
    
    @Test
    public void listAllAuthors() throws Exception {
        String result = mvc.perform(
                    get("/rest/authors/list")
                          .headers(generateHeaders())
                          .contentType(MediaType.APPLICATION_JSON)
                          .accept(MediaType.APPLICATION_JSON)
                                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
//              .andExpect(jsonPath("$.*", hasSize(5)))
              .andReturn()
              .getResponse()
              .getContentAsString();
        
        //System.out.println(result);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        
        List<Author> authors = mapper.readValue(result, new TypeReference<List<Author>>() {});
        System.out.println(authors.size());
        //assertEquals(5, authors.size());
        //assertTrue(authors.size() > 0);
    }
    
    @Test
    public void createAuthor() throws Exception {
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorFIO("TESTEROV TEST TESTEROVICH");
        authorDTO.setLifePeriod("2022-2023");
        authorDTO.setDescription("Author for TEST PURPOSE");
        //authorDTO.setId(null);
        
        String response = mvc.perform(
                    post("/rest/authors/add")
                          .headers(generateHeaders())
                          .contentType(MediaType.APPLICATION_JSON)
                          .content(asJsonString(authorDTO))
                          .accept(MediaType.APPLICATION_JSON)
                                     )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
              .andReturn()
              .getResponse()
              .getContentAsString();
        
        System.out.println(response);
        ObjectMapper objectMapper = new ObjectMapper();
        Author author = objectMapper.readValue(response, Author.class);
        System.out.println(author.getId());
    }
    
    @Test
    public void updateAuthor() throws Exception {
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setAuthorFIO("TESTEROV TEST TESTEROVICH UPDATED");
        authorDTO.setLifePeriod("2022-2023 UPD");
        authorDTO.setDescription("Author for TEST PURPOSE UPD");
        //authorDTO.setId(null);
        
        String response = mvc.perform(
                    put("/rest/authors/update")
                          .headers(generateHeaders())
                          .contentType(MediaType.APPLICATION_JSON)
                          .content(asJsonString(authorDTO))
                          .accept(MediaType.APPLICATION_JSON)
                                     )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
              .andReturn()
              .getResponse()
              .getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        Author author = objectMapper.readValue(response, Author.class);
        System.out.println(author);
    }
    
    @Test
    public void deleteAuthor() throws Exception {
        String response = mvc.perform(
                    delete("/rest/authors/delete")
                          .param("authorId", "22")
                          .headers(generateHeaders())
                          .contentType(MediaType.APPLICATION_JSON)
                          .accept(MediaType.APPLICATION_JSON)
                                     )
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andReturn()
              .getResponse()
              .getContentAsString();
        
        System.out.println(response);
    }
    
    
    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
