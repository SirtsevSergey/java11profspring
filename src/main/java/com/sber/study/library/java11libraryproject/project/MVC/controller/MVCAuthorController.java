package com.sber.study.library.java11libraryproject.project.MVC.controller;

import com.sber.study.library.java11libraryproject.project.dto.AuthorDTO;
import com.sber.study.library.java11libraryproject.project.service.AuthorService;
import com.sber.study.library.java11libraryproject.project.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/authors")
public class MVCAuthorController {
    private final AuthorService authorService;
    private final BookService bookService;
    
    public MVCAuthorController(AuthorService authorService,
                               BookService bookService) {
        this.authorService = authorService;
        this.bookService = bookService;
    }
    
    @GetMapping("")
    public String getAll(Model model) {
        //TODO: добавить нужгую модель/ДТО для вывода информации по авторам
        model.addAttribute("authors", "?");
        //TODO: нарисовать страницу
        return "authors/viewAllAuthors";
    }
    
    @GetMapping("/{authorId}")
    public String getAuthorWithBooks(@PathVariable Long authorId, Model model) {
        //some code here
        model.addAttribute("authorWithBooks", "?");
        //TODO: нарисовать страницу по просмотру конкретного автора и информации о его книгах
        return "authors/viewAuthor";
    }
    
    @GetMapping("/add")
    public String create() {
        return "/authors/addAuthor";
    }
    
    @PostMapping("/add")
    public String create(@ModelAttribute("authorForm") @Valid AuthorDTO authorDTO) {
        return "redirect:/authors";
    }
    
    @GetMapping("/add-book/{authorId}")
    public String createBookToAuthor(@PathVariable Long authorId, Model model) {
        //показать форму для добавления книги существующей к автору
        return "/authors/addAuthorBook";
    }
    
    /*
    Нужен еще один метод для добавления книги автору (какой?) Нужно его реализовать
     */
    
    @PostMapping("/search")
    public String searchByAuthorFIO(@ModelAttribute("authorForm") @Valid AuthorDTO authorDTO,
                                    Model model) {
        model.addAttribute("authors", "?");
        return "authors/viewAllAuthors";
    }
    
}
