package com.sber.study.library.java11libraryproject.project.repository;

import com.sber.study.library.java11libraryproject.project.model.Author;
import com.sber.study.library.java11libraryproject.project.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface AuthorRepository
      extends JpaRepository<Author, Long> {
    List<Author> findAuthorByBooksIn(Set<Book> books);
    
    List<Author> findAuthorByBooks(Book book);
    
    List<Author> findAuthorByBooksId(Long bookId);
}
