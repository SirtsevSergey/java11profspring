package com.sber.study.library.java11libraryproject.project.MVC.controller;

import com.sber.study.library.java11libraryproject.project.dto.PublishDTO;
import com.sber.study.library.java11libraryproject.project.model.Book;
import com.sber.study.library.java11libraryproject.project.model.User;
import com.sber.study.library.java11libraryproject.project.service.BookService;
import com.sber.study.library.java11libraryproject.project.service.PublishService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@Slf4j
@RequestMapping("/publish")
public class MVCPublishController {
    
    private BookService bookService;
    private PublishService publishService;
    
    @Autowired
    public void getBookService(BookService bookService) {
        this.bookService = bookService;
    }
    
    @Autowired
    public void getPublishService(PublishService publishService) {
        this.publishService = publishService;
    }
    
    @GetMapping("/user-books/{id}")
    public String userBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "10") int pageSize,
                            @PathVariable Long id,
                            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "book.title"));
        Page<PublishDTO> result = publishService.getUserPublishing(id, pageRequest);
        User user = result.getContent().get(0).getUser();
        model.addAttribute("publish", result);
        model.addAttribute("userId", id);
        model.addAttribute("userFio", user.getLastName() + " " + user.getFirstName());
        return "userBooks/viewAllUserBooks";
    }
    
    @GetMapping("/get-book/{id}")
    public String getBook(@PathVariable Long id,
                          Model model) {
        Book book = bookService.getOne(id);
        model.addAttribute("book", book);
        return "userBooks/getBook";
    }
    
    @PostMapping("/get-book")
    public String getBook(HttpServletRequest httpServletRequest,
                          HttpServletResponse httpServletResponse) {
        SecurityContext context = SecurityContextHolder.getContext();
        Long bookId = Long.valueOf(httpServletRequest.getParameter("bookId"));
        long count = Long.parseLong(httpServletRequest.getParameter("count"));
        String periodStr = httpServletRequest.getParameter("period");
        int period = Integer.parseInt(periodStr.isEmpty() ? "3" : periodStr);
        log.debug("period: " + period);
        publishService.rentABook(bookId, context.getAuthentication().getName(), (int) count, period);
        return "redirect:/books";
    }
    
    @PostMapping("/user-books/{id}/search")
    public String searchPublish(@PathVariable Long id,
                                @RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "10") int pageSize,
                                @ModelAttribute("publishSearchForm") PublishDTO publishDTO,
                                Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "rentDate"));
        Page<PublishDTO> result = publishService.searchPublish(publishDTO, pageRequest);
        User user = result.getContent().get(0).getUser();
        model.addAttribute("publish", result);
        model.addAttribute("userId", id);
        model.addAttribute("userFio", user.getLastName() + " " + user.getFirstName());
        return "userBooks/viewAllUserBooks";
    }
    
    @GetMapping("/return-book/{id}")
    public String returnBook(@PathVariable Long id,
                             Model model) {
        publishService.returnBook(id);
        return "redirect:/publish/user-books/" + publishService.getOne(id).getUser().getId();
    }
}
