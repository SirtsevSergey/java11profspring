package com.sber.study.library.java11libraryproject.project.controller;

import com.sber.study.library.java11libraryproject.project.dto.AuthorDTO;
import com.sber.study.library.java11libraryproject.project.dto.BookAuthorDTO;
import com.sber.study.library.java11libraryproject.project.dto.BookDTO;
import com.sber.study.library.java11libraryproject.project.exception.MyDeleteException;
import com.sber.study.library.java11libraryproject.project.model.Author;
import com.sber.study.library.java11libraryproject.project.model.Book;
import com.sber.study.library.java11libraryproject.project.repository.AuthorRepository;
import com.sber.study.library.java11libraryproject.project.repository.BookRepository;
import com.sber.study.library.java11libraryproject.project.service.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/books")
//CORS Filters
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Книги",
     description = "Контроллер для работы с книгами нашей библиотеки.")
public class BookController {

//    private final BookRepository bookRepository;
//    private final AuthorRepository authorRepository;
    
    private final GenericService<Book, BookDTO> bookService;
    private final GenericService<Author, AuthorDTO> authorService;

//    public BookController(BookRepository bookRepository,
//                          AuthorRepository authorRepository) {
//        this.bookRepository = bookRepository;
//        this.authorRepository = authorRepository;
//    }
    
    public BookController(GenericService<Book, BookDTO> bookService,
                          GenericService<Author, AuthorDTO> authorService) {
        this.bookService = bookService;
        this.authorService = authorService;
    }
    
    @Operation(description = "Получить информацию об одной книге по ее ID",
               method = "getOne")
    @RequestMapping(value = "/getBook", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> getOne(@RequestParam(value = "bookId") Long bookId) {
        return ResponseEntity.status(HttpStatus.OK)
              .body(bookService.getOne(bookId));
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Book>> listAllBooks() {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.listAll());
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> add(@RequestBody BookAuthorDTO newBook) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.createFromDTO(newBook));
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> updateBook(@RequestBody BookAuthorDTO book,
                                           @RequestParam(value = "bookId") Long bookId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.updateFromDTO(book, bookId));
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "bookId") Long bookId) throws MyDeleteException {
        bookService.delete(bookId);
        return ResponseEntity.status(HttpStatus.OK).body("Книга успешно удалена");
    }
    
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> addAuthor(@RequestParam(value = "bookId") Long bookId,
                                          @RequestParam(value = "authorId") Long authorId) {
//        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundException("Such author with id=" + authorId + " was not
//        found"));
//        Book book = bookRepository.findById(bookId).orElseThrow(() -> new NotFoundException("Such book with id=" + bookId + " was not found"));
        
        Author author = authorService.getOne(authorId);
        Book book = bookService.getOne(bookId);
        
        book.getAuthors().add(author);
        
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.update(book));
        
    }
    
}
