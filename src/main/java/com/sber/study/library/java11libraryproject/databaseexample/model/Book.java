package com.sber.study.library.java11libraryproject.databaseexample.model;

import lombok.*;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    //@Setter(AccessLevel.NONE)
    private Integer id;
    private String author;
    private String title;
    private String date;
}
