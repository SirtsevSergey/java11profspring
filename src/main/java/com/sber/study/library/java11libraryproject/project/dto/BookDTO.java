package com.sber.study.library.java11libraryproject.project.dto;

import com.sber.study.library.java11libraryproject.project.model.Book;
import com.sber.study.library.java11libraryproject.project.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO
      extends CommonDTO {
    
    private Long id;
    private String title;
    private Genre genre;
    private String onlineCopy;
    private String storagePlace;
    private Integer amount;
    private String publishYear;
    
    public BookDTO(final Book book) {
        this.id = book.getId();
        this.amount = book.getAmount();
        this.genre = book.getGenre();
        this.onlineCopy = book.getOnlineCopy();
        this.publishYear = book.getPublishYear();
        this.storagePlace = book.getStoragePlace();
        this.title = book.getTitle();
    }
}
