package com.sber.study.library.java11libraryproject.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "publishing")
@SequenceGenerator(name = "default_gen", sequenceName = "publishing_seq", allocationSize = 1)
public class Publish
      extends GenericModel {
    
    @Column(name = "rent_date", nullable = false)
    private LocalDateTime rentDate;
    
    @Column(name = "return_date", nullable = false)
    private LocalDateTime returnDate;
    
    @Column(name = "returned", nullable = false)
    private boolean returned;
    
    @Column(name = "rent_period", nullable = false)
    private Integer rentPeriod;
    
    @Column(name = "amount")
    private Integer amount;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_PUBLISHING_USER"))
    @JsonIgnore
    private User user;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "book_id", foreignKey = @ForeignKey(name = "FK_PUBLISHING_BOOK"))
    @JsonIgnore
    private Book book;
}
