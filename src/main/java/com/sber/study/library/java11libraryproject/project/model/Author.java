package com.sber.study.library.java11libraryproject.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "authors")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "authors_seq", allocationSize = 1)
public class Author
      extends GenericModel {
    
    @Column(name = "author_fio")
    private String authorFIO;
    
    @Column(name = "life_period")
    private String lifePeriod;
    
    @Column(name = "description")
    private String description;
    
    //подумать какой нужен fetchType и cascade
    @JsonIgnore
    @ManyToMany(mappedBy = "authors", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @ToString.Exclude
    private Set<Book> books = new HashSet<>();
}
