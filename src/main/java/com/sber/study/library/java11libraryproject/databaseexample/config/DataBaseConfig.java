//package com.sber.study.library.java11libraryproject.databaseexample.config;
//
//import com.sber.study.library.java11libraryproject.databaseexample.dao.BookDAO2;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//import static com.sber.study.library.java11libraryproject.databaseexample.DBConstants.*;
//
//@Configuration
//public class DataBaseConfig {
//
////    @Bean
////    @Scope("singleton")
////    //@Scope("prototype") etc.
////    public Connection connection() throws SQLException {
////        return DriverManager.getConnection(
////              "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
////              USER,
////              PASSWORD);
////    }
//
//    @Bean
//    public BookDAO2 bookDAO2() throws SQLException {
//        return new BookDAO2(connection());
//    }
//}
