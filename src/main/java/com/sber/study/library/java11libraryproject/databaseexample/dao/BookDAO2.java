package com.sber.study.library.java11libraryproject.databaseexample.dao;

import com.sber.study.library.java11libraryproject.databaseexample.model.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookDAO2 {
    
    private Connection connection;
    
    public BookDAO2(Connection connection) {
        this.connection = connection;
    }
    
    public Book findBookById(final Integer id) throws SQLException {
        PreparedStatement query = connection.prepareStatement("select * from books where id = ?");
        query.setInt(1, id);
        ResultSet resultSet = query.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setId(resultSet.getInt("id"));
            book.setAuthor(resultSet.getString("author"));
            book.setTitle(resultSet.getString("title"));
            book.setDate(resultSet.getString("date_added"));
            System.out.println(book);
        }
        return book;
    }
}
