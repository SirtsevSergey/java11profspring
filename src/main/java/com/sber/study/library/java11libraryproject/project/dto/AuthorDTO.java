package com.sber.study.library.java11libraryproject.project.dto;

import com.sber.study.library.java11libraryproject.project.model.Author;
import com.sber.study.library.java11libraryproject.project.model.Book;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class AuthorDTO
      extends CommonDTO {
    private Long id;
    private String authorFIO;
    private String lifePeriod;
    private String description;
    
    public AuthorDTO(final Author author) {
        this.id = author.getId();
        this.authorFIO = author.getAuthorFIO();
        this.lifePeriod = author.getLifePeriod();
        this.description = author.getDescription();
    }
}
