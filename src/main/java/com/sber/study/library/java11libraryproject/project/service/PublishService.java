package com.sber.study.library.java11libraryproject.project.service;

import com.sber.study.library.java11libraryproject.project.dto.PublishDTO;
import com.sber.study.library.java11libraryproject.project.dto.UserPublishDTO;
import com.sber.study.library.java11libraryproject.project.exception.MyDeleteException;
import com.sber.study.library.java11libraryproject.project.model.Book;
import com.sber.study.library.java11libraryproject.project.model.Publish;
import com.sber.study.library.java11libraryproject.project.model.User;
import com.sber.study.library.java11libraryproject.project.repository.PublishRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class PublishService
      extends GenericService<Publish, PublishDTO> {
    private final UserService userService;
    private final BookService bookService;
    
    private final PublishRepository publishRepository;
    
    public PublishService(UserService userService,
                          BookService bookService,
                          PublishRepository publishRepository) {
        this.userService = userService;
        this.bookService = bookService;
        this.publishRepository = publishRepository;
    }
    
    public Page<PublishDTO> getUserPublishing(Long id,
                                              Pageable pageable) {
        UserPublishDTO userPublishDTO = new UserPublishDTO(userService.getOne(id));
        return new PageImpl<>(new ArrayList<>(userPublishDTO.getPublishDTOSet()),
                              pageable,
                              userPublishDTO.getPublishDTOSet().size());
    }
    
    public Page<PublishDTO> searchPublish(PublishDTO publishDTO,
                                          Pageable pageable) {
        List<Publish> publishes = publishRepository.findAllByBookTitleContainingIgnoreCase(publishDTO.getBookTitleSearch());
        List<PublishDTO> publishDTOS = new ArrayList<>();
        for (Publish p : publishes) {
            publishDTOS.add(new PublishDTO(p));
        }
        return new PageImpl<>(publishDTOS, pageable, publishDTOS.size());
    }
    
    public void rentABook(Long bookId,
                          String name,
                          int count,
                          int period) {
        User user = userService.getByUserName(name);
        Book book = bookService.getOne(bookId);
        book.setAmount(book.getAmount() - count);
        bookService.update(book);
        
        PublishDTO publishDTO = new PublishDTO(user, book, count, period);
        Publish publish = new Publish();
        publish.setBook(publishDTO.getBook());
        publish.setUser(publishDTO.getUser());
        publish.setRentDate(publishDTO.getRentDate());
        publish.setRentPeriod(publishDTO.getRentPeriod());
        publish.setReturned(publishDTO.isReturned());
        publish.setReturnDate(publishDTO.getReturnDate());
        publish.setAmount(publishDTO.getAmount());
        publishRepository.save(publish);
    }
    
    public void returnBook(Long publishId) {
        Publish publish = getOne(publishId);
        publish.setReturned(true);
        Book book = publish.getBook();
        book.setAmount(book.getAmount() + publish.getAmount());
        publishRepository.save(publish);
        bookService.update(book);
    }
    
    @Override
    public Publish update(Publish object) {
        return null;
    }
    
    @Override
    public Publish updateFromDTO(PublishDTO object, Long authorId) {
        return null;
    }
    
    @Override
    public Publish createFromDTO(PublishDTO newDtoObject) {
        return null;
    }
    
    @Override
    public Publish createFromEntity(Publish newObject) {
        return null;
    }
    
    @Override
    public void delete(Long objectId) throws MyDeleteException {
    
    }
    
    @Override
    public Publish getOne(Long objectId) {
        return publishRepository.findById(objectId).orElseThrow(() -> new NotFoundException("Such Publish does not exist"));
    }
    
    @Override
    public List<Publish> listAll() {
        return publishRepository.findAll();
    }
}
