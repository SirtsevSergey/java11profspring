package com.sber.study.library.java11libraryproject.project.dto;

import com.sber.study.library.java11libraryproject.project.model.Publish;
import com.sber.study.library.java11libraryproject.project.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class UserPublishDTO
      extends UserDTO {
    private Set<PublishDTO> publishDTOSet;
    
    public UserPublishDTO(User user) {
        super(user);
        Set<Publish> publishes = user.getPublish();
        Set<PublishDTO> publishDTOS = new HashSet<>();
        for (Publish publish : publishes) {
            PublishDTO publishDTO = new PublishDTO(publish);
            publishDTOS.add(publishDTO);
        }
        this.publishDTOSet = publishDTOS;
    }
}
