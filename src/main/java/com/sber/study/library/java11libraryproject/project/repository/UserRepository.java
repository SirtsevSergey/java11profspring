package com.sber.study.library.java11libraryproject.project.repository;

import com.sber.study.library.java11libraryproject.project.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository
      extends JpaRepository<User, Long> {
    
    @Query(value = "select * from users as u where (select p.id from publishing as p ) = u.id)",
           nativeQuery = true)
    List<User> getAllUserWithBooks();
    
    User findUserByLogin(String login);
    
    User findByBackUpEmail(String email);
    
    @Query(nativeQuery = true,
           value = """
                 select backup_email
                 from users u join publishing p on u.id = p.user_id
                 where p.return_date >= now()
                 and p.returned = false
                                  """)
    List<String> getDelayedEmails();
}
