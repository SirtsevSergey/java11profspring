package com.sber.study.library.java11libraryproject.project.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "books")
@NoArgsConstructor
@Getter
@Setter
@ToString
@SequenceGenerator(name = "default_gen", sequenceName = "books_seq", allocationSize = 1)
public class Book
      extends GenericModel {
    
    @Column(name = "title", nullable = false)
    private String title;
    
    @Column(name = "online_copy")
    private String onlineCopy;
    
    @Column(name = "genre")
    @Enumerated
    private Genre genre;
    
    @Column(name = "place")
    private String storagePlace;
    
    @Column(name = "amount")
    private Integer amount;
    
    @Column(name = "publish_year")
    private String publishYear;
    
    //подумать какой нужен fetchType и cascade
    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "books_authors",
               joinColumns = @JoinColumn(name = "book_id"), foreignKey = @ForeignKey(name = "FK_BOOKS_AUTHORS"),
               inverseJoinColumns = @JoinColumn(name = "author_id"), inverseForeignKey = @ForeignKey(name = "FK_AUTHORS_BOOKS"))
    @ToString.Exclude
//    @JsonIgnore
    private Set<Author> authors = new HashSet<>();
    
    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    private Set<Publish> publish;
}
