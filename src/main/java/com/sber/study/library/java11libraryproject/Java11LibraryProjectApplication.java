package com.sber.study.library.java11libraryproject;

import com.sber.study.library.java11libraryproject.databaseexample.dao.BookDAO2;

import com.sber.study.library.java11libraryproject.databaseexample.model.Book;
import com.sber.study.library.java11libraryproject.project.model.Author;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.List;

@SpringBootApplication
@EnableScheduling
public class Java11LibraryProjectApplication
      implements CommandLineRunner {
    
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    public Java11LibraryProjectApplication(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public static void main(String[] args) {
        SpringApplication.run(Java11LibraryProjectApplication.class, args);
    }
    
    @Override
    public void run(String... args) throws Exception {
//        BookDAO bookDAO = new BookDAO();
//        bookDAO.findById(1);

//        BookDAO2 bookDAO2 = new BookDAO2(DbApp.INSTANCE.newConnection());
//        bookDAO2.findBookById(2);

//        ApplicationContext ctx = new AnnotationConfigApplicationContext(DataBaseConfig.class);
//        //ClassPathXmlApplicationContext
//        BookDAO2 bookDAO2 = ctx.getBean(BookDAO2.class);
//        bookDAO2.findBookById(1);
//
//        List<Book> books = jdbcTemplate.query("select * from books",
//                                              (rs, rowNUm) -> new Book(
//                                                    rs.getInt(1),
//                                                    rs.getString(2),
//                                                    rs.getString(3),
//                                                    rs.getString(4)
//                                              ));
//        books.forEach(System.out::println);
        
    }
}
