package com.sber.study.library.java11libraryproject.databaseexample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.sber.study.library.java11libraryproject.databaseexample.DBConstants.*;

public enum DbApp {
    INSTANCE;
    private Connection connection;
    
    public Connection newConnection() throws SQLException {
        return DriverManager.getConnection(
              "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB,
              USER,
              PASSWORD);
    }
}
