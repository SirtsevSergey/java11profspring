package com.sber.study.library.java11libraryproject.project.dto;

import com.sber.study.library.java11libraryproject.project.model.Genre;
import lombok.Data;

@Data
public class BookSearchDTO {
    private String bookTitle;
    private String authorFio;
    private Genre genre;
}
