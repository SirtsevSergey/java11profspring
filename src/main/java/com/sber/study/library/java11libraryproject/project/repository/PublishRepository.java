package com.sber.study.library.java11libraryproject.project.repository;

import com.sber.study.library.java11libraryproject.project.model.Publish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublishRepository extends JpaRepository<Publish, Long> {

    Publish getPublishByUserId(Long userId);

    List<Publish> findAllByBookTitleContainingIgnoreCase(String title);
}
