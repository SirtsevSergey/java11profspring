package com.sber.study.library.java11libraryproject.project.exception;

public class MyDeleteException
      extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
