package com.sber.study.library.java11libraryproject.project.controller;

import com.sber.study.library.java11libraryproject.project.dto.LoginDTO;
import com.sber.study.library.java11libraryproject.project.jwtsecurity.JwtTokenUtil;
import com.sber.study.library.java11libraryproject.project.service.userdetails.CustomUserDetailsService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    private final CustomUserDetailsService authenticationService;
    private final JwtTokenUtil jwtTokenUtil;
    
    
    public UserController(CustomUserDetailsService authenticationService,
                          JwtTokenUtil jwtTokenUtil) {
        this.authenticationService = authenticationService;
        this.jwtTokenUtil = jwtTokenUtil;
    }
    
    @RequestMapping(value = "/auth", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        HashMap<String, Object> response = new HashMap<>();
        UserDetails foundUser = authenticationService.loadUserByUsername(loginDTO.getUsername());
        String token = jwtTokenUtil.generateToken(foundUser);
        response.put("token", token);
        //System.out.println("ROLES IN TOKEN: " + jwtTokenUtil.getUserRoleFromToken(token));
        return ResponseEntity.ok().body(response);
    }
}
